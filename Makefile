include ./build/.env

download:
	docker build -t softpro/downloader -f ./build/Dockerfile.downloader .

build: download
	docker build -t softpro/builder -f ./build/Dockerfile.builder . && \
	docker build -t softpro/executor -f ./build/Dockerfile .

lint: build
	docker run softpro/builder golint ./...

tests: build
	docker run -v $(shell pwd)/configs/testing_logger.json:/softpro/testing_logger.json \
		-e TEST_LOGGER_CONFIG_PATH=/softpro/testing_logger.json \
		softpro/builder go test -count=1 ./...

run: build
	docker-compose -f ./build/docker-compose.run.yml up -d

stop:
	docker-compose -f ./build/docker-compose.run.yml stop