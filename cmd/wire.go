//go:generate wire
// +build wireinject

package main

import (
	"github.com/google/wire"
	"softpro/internal/app"
	"softpro/internal/configs"
	"softpro/internal/databases"
	"softpro/internal/lines"
	"softpro/internal/repositories/redisrepo"
	"softpro/internal/repositories/soccer"
	"softpro/internal/servers"
	"softpro/internal/services/liner"
	"softpro/internal/services/monitor"
)

func Inject(configPath string) (*app.App, func(), error) {
	wire.Build(
		configs.ConfigProviders,
		wire.FieldsOf(new(*configs.Full), "DB"),
		wire.FieldsOf(new(*configs.Full), "Net"),
		wire.FieldsOf(new(*configs.Net), "LinesProvider"),
		wire.FieldsOf(new(*configs.DB), "Pg", "Redis"),
		databases.DBProviders,
		wire.Bind(new(monitor.ConnectionPool), new(*databases.ConnectionPool)),
		monitor.ProviderOfMonitor,
		wire.Bind(new(soccer.DBPool), new(*databases.PgContext)),
		soccer.ProviderSoccer,
		wire.Bind(new(redisrepo.ConnectionPool), new(*databases.RedisContext)),
		redisrepo.ProviderRedisRepos,
		wire.Bind(new(liner.SoccerRepo), new(*soccer.Repository)),
		wire.Bind(new(liner.FootballRepo), new(*redisrepo.FootballRepository)),
		wire.Bind(new(liner.BaseballRepo), new(*redisrepo.BaseballRepository)),
		servers.ProviderServers,
		liner.ProvideSubscriber,
		wire.Bind(new(lines.SoccerRepository), new(*soccer.Repository)),
		wire.Bind(new(lines.FootballRepository), new(*redisrepo.FootballRepository)),
		wire.Bind(new(lines.BaseballRepository), new(*redisrepo.BaseballRepository)),
		lines.ProvidePuller,
		app.ProvideApp,
	)
	return nil, nil, nil
}