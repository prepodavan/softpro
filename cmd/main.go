package main

import (
	"errors"
	"flag"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	cfgPathPtr := flag.String("config_path", "", "path to config file; can not be empty")
	flag.Parse()
	cfgPath := *cfgPathPtr
	if len(cfgPath) == 0 {
		panic(errors.New("no config file provided"))
	}

	appInstance, cleanup, err := Inject(cfgPath)
	if err != nil {
		panic(err)
	}

	defer appInstance.WG.Wait()
	defer cleanup()
	signalDone := make(chan os.Signal, 1)
	doneWithError := make(chan error, 10)
	poolConnected := make(chan struct{}, 1)
	signal.Notify(signalDone, os.Interrupt, syscall.SIGTERM, syscall.SIGINT)
	go appInstance.ServeHttp(doneWithError)
	appInstance.ConnectPool(doneWithError, poolConnected)
	go appInstance.Puller.Run()
	for {
		select {
		case <-doneWithError:
			appInstance.Logger.Error("shutting down cz of error")
			return
		case <-signalDone:
			appInstance.Logger.Info("shutting down")
			return
		case <-poolConnected:
			go appInstance.ServeGrpc(doneWithError)
		}
	}
}
