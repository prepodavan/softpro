FROM softpro/downloader AS downloader
FROM golang:alpine AS builder

ENV GOOS=linux
ENV CGO_ENABLED=0
ENV GO111MODULE=on

WORKDIR /softpro
COPY --from=downloader /go/bin /go/bin
COPY --from=downloader /go/pkg/mod/cache /go/pkg/mod/cache
COPY go.mod .
COPY go.sum .
COPY cmd cmd
COPY internal internal

RUN go generate ./...
RUN go build -ldflags="-w -s" -o softpro_app softpro/cmd