package databases

import (
	"sync"

	"go.uber.org/zap"
)

type ConnectionPool struct {
	conns  []DBConnection
	logger *zap.Logger
}

func (cp *ConnectionPool) Ping() (output []error) {
	return cp.call(func(conn DBConnection) error {
		return conn.Ping()
	})
}

func (cp *ConnectionPool) Connect() []error {
	return cp.call(func(conn DBConnection) error {
		return conn.Connect()
	})
}

func (cp *ConnectionPool) Close() []error {
	cp.logger.Info("closing db pool")
	errs := cp.call(func(conn DBConnection) error {
		return conn.Close()
	})

	cp.logger.Info("db pool closed")
	return errs
}

func (cp *ConnectionPool) call(caller func(DBConnection) error) (output []error) {
	wg := sync.WaitGroup{}
	trash := make(chan error, len(cp.conns))
	output = make([]error, 0, len(cp.conns))
	for i := range cp.conns {
		wg.Add(1)
		go func(conn DBConnection) {
			defer wg.Done()
			trash <- caller(conn)
		}(cp.conns[i])
	}

	wg.Wait()
	for range cp.conns {
		err := <-trash
		if err != nil {
			output = append(output, err)
		}
	}

	return
}
