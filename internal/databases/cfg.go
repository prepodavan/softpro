package databases

import "time"

type SportLinesDBsConfig struct {
	pgPingTimeout time.Duration
	redisPingTimeout time.Duration
}