package databases

import "errors"

var (
	ErrPingPg    = errors.New("pinged pg with error")
	ErrPingRedis = errors.New("pinged redis with error")
	ErrNotConnected = errors.New("not connected")
)