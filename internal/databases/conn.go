package databases

type DBConnection interface {
	Ping() error
	Close() error
	Connect() error
}