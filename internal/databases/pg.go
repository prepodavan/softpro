package databases

import (
	"database/sql"
	"fmt"
	"sync"

	_ "github.com/lib/pq" // register driver
	"go.uber.org/zap"
)

type PgContext struct {
	mx     sync.RWMutex
	db     *sql.DB
	Logger *zap.Logger
	Cfg    *PgConfig
}

type PgConfig struct {
	User     string
	Password string
	DBName   string
	SSLMode  string
	Host     string
}

func (pgctx *PgContext) GetPostgres() *sql.DB {
	pgctx.mx.RLock()
	db := pgctx.db
	pgctx.mx.RUnlock()
	return db
}

func (pgctx *PgContext) Ping() error {
	pgctx.mx.RLock()
	db := pgctx.db
	pgctx.mx.RUnlock()
	if db == nil {
		return ErrNotConnected
	}

	return db.Ping()
}

func (pgctx *PgContext) Connect() error {
	connStr := fmt.Sprintf(
		"postgres://%s:%s@%s/%s?sslmode=%s",
		pgctx.Cfg.User,
		pgctx.Cfg.Password,
		pgctx.Cfg.Host,
		pgctx.Cfg.DBName,
		pgctx.Cfg.SSLMode,
	)

	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return err
	}

	err = db.Ping()
	if err != nil {
		return err
	}

	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS soccer_line(
		id serial, value NUMERIC(10, 5) not null, created_at timestamp not null)`)
	if err != nil {
		return err
	}

	pgctx.mx.Lock()
	pgctx.db = db
	pgctx.mx.Unlock()
	return nil
}

func (pgctx *PgContext) Close() error {
	pgctx.mx.RLock()
	db := pgctx.db
	pgctx.mx.RUnlock()
	if db == nil {
		return ErrNotConnected
	}

	return db.Close()
}
