package databases

import (
	"github.com/go-redis/redis"
	"go.uber.org/zap"
	"sync"
)

type RedisContext struct {
	mx     sync.RWMutex
	client *redis.Client
	Logger *zap.Logger
	Cfg    *RedisConfig
}

type RedisConfig struct {
	DB       int
	Addr     string
	Password string
}

func (rctx *RedisContext) GetRedis() *redis.Client {
	rctx.mx.RLock()
	db := rctx.client
	rctx.mx.RUnlock()
	return db
}

func (rctx *RedisContext) Ping() error {
	rctx.mx.RLock()
	client := rctx.client
	rctx.mx.RUnlock()
	if client == nil {
		return ErrNotConnected
	}

	return client.Ping().Err()
}

func (rctx *RedisContext) Connect() error {
	client := redis.NewClient(&redis.Options{
		Addr:     rctx.Cfg.Addr,
		Password: rctx.Cfg.Password,
		DB:       rctx.Cfg.DB,
	})

	rctx.mx.Lock()
	rctx.client = client
	rctx.mx.Unlock()

	return client.Ping().Err()
}

func (rctx *RedisContext) Close() error {
	rctx.mx.RLock()
	client := rctx.client
	rctx.mx.RUnlock()
	if client == nil {
		return ErrNotConnected
	}

	return client.Close()
}
