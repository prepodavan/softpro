package databases

import (
	"github.com/google/wire"
	"go.uber.org/zap"
)

var DBProviders = wire.NewSet(
	wire.Struct(new(PgContext), "Logger", "Cfg"),
	wire.Struct(new(RedisContext), "Logger", "Cfg"),
	NewConnectionPool,
)

func NewConnectionPool(logger *zap.Logger, r *RedisContext, p *PgContext) (cp *ConnectionPool, cleanup func()) {
	cp = &ConnectionPool{
		conns:  []DBConnection{r, p},
		logger: logger,
	}

	cleanup = func() {
		cp.Close() // TODO log
	}

	return
}