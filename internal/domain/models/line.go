package models

import "time"

type Line struct {
	Sport   string
	Value   float64
	Instant time.Time
}
