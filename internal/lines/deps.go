package lines

import (
	"softpro/internal/domain/models"
)

type ProviderConfig struct {
	BaseURL string
	Lines   []*LineConfig
}

type LineConfig struct {
	Sport string
	IntervalInSeconds int64
}

type SportLineStorage interface {
	Save(line models.Line) error
}
