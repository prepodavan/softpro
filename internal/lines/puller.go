package lines

import (
	"encoding/json"
	"go.uber.org/zap"
	"net/http"
	"softpro/internal/domain/models"
	"strconv"
	"strings"
	"sync"
	"time"
)

type linePuller struct {
	url        string
	sportField string
	logger     *zap.Logger
	config     *pullerConfig
	closing    chan struct{}
	wg         *sync.WaitGroup
}

type pullerConfig struct {
	Sport    string
	Interval time.Duration
	Storage  SportLineStorage
	Client   *http.Client
}

func newLinePuller(baseURL string, wg *sync.WaitGroup, logger *zap.Logger, config *pullerConfig) *linePuller {
	return &linePuller{
		url:        baseURL + "/" + strings.ToLower(config.Sport),
		sportField: strings.ToUpper(config.Sport),
		logger:     logger,
		config:     config,
		closing:    make(chan struct{}, 1),
		wg:         wg,
	}
}

func (puller *linePuller) run() {
	ticker := time.NewTicker(puller.config.Interval)
	puller.wg.Add(1)
	defer ticker.Stop()
	defer puller.wg.Done()

	for {
		select {
		case <-ticker.C:
			go puller.pull()
		case <-puller.closing:
			return
		}
	}
}

func (puller *linePuller) close() {
	puller.closing <- struct{}{}
}

func (puller *linePuller) pull() (err error) {
	puller.wg.Add(1)
	defer puller.wg.Done()

	sportLogger := puller.logger.With(zap.String("sport", puller.config.Sport))
	sportLogger.Debug("getting sport")
	var response *providerResponse
	response, err = puller.fetch()
	if err != nil {
		sportLogger.Error("failed to get next sport line event", zap.Error(err))
		return
	}

	sportLogger.Debug("parsing response of provider")
	var event models.Line
	event, err = puller.parse(response)
	if err != nil {
		sportLogger.Error("failed to parse response of provider", zap.Error(err))
		return
	}

	eventLogger := puller.logger.With(
		zap.String("event.sport", event.Sport),
		zap.Time("event.created_at", event.Instant),
		zap.Float64("event.value", event.Value),
	)

	eventLogger.Debug("saving")
	err = puller.config.Storage.Save(event)
	if err != nil {
		eventLogger.Error("failed to save provided event", zap.Error(err))
		return
	}

	return
}

func (puller *linePuller) fetch() (response *providerResponse, err error) {
	var res *http.Response
	logger := puller.logger.With(
		zap.String("sport", puller.config.Sport),
		zap.String("url", puller.url),
	)

	res, err = puller.config.Client.Get(puller.url)
	if err != nil {
		err = &ErrGetLine{
			Reason: err,
			Sport:  puller.config.Sport,
		}

		logger.Debug("GET /<sport> with error", zap.Error(err))
		return
	}

	var payload providerResponse
	defer res.Body.Close()
	err = json.NewDecoder(res.Body).Decode(&payload)
	if err != nil {
		err = &ErrLineDecoding{
			Reason: err,
			Sport:  puller.config.Sport,
		}

		logger.Debug("error decoding provider response", zap.Error(err))
		return
	}

	response = &payload
	return
}

func (puller *linePuller) parse(response *providerResponse) (event models.Line, err error) {
	var ok bool
	var strValue string
	if strValue, ok = response.Lines[puller.sportField]; !ok {
		err = &ErrUnexpectedProviderResponse{
			Response:      response,
			ExpectedSport: &puller.sportField,
		}

		puller.logger.Debug(
			"expected field not found in provider response",
			zap.Error(err),
			zap.String("expected field", puller.sportField),
			zap.Any("lines of response", response.Lines),
		)

		return
	}

	var floatValue float64
	if floatValue, err = strconv.ParseFloat(strValue, 64); err != nil {
		err = &ErrUnexpectedProviderResponse{
			Response:      response,
			ExpectedSport: &puller.sportField,
			InvalidValue:  &strValue,
		}

		puller.logger.Debug(
			"cant parse float value from provider response",
			zap.Error(err),
			zap.String("field", puller.sportField),
			zap.String("value", strValue),
		)

		return
	}

	event = models.Line{
		Sport:   puller.config.Sport,
		Value:   floatValue,
		Instant: time.Now(),
	}

	return
}
