package lines

import (
	"fmt"
)

type ErrUnknownSport struct {
	Sport string
}

func (err *ErrUnknownSport) Error() string {
	return fmt.Sprintf("sport {%s} is unknown", err.Sport)
}

type ErrLineDecoding struct {
	Reason error
	Sport string
}

func (err *ErrLineDecoding) Error() string {
	return fmt.Sprintf("failed to decode line of %s sport: %s", err.Sport, err.Reason.Error())
}

type ErrGetLine struct {
	Reason error
	Sport string
}

func (err *ErrGetLine) Error() string {
	return fmt.Sprintf("failed to GET /<%s> line: %s", err.Sport, err.Reason.Error())
}

type ErrUnexpectedProviderResponse struct {
	Response *providerResponse
	ExpectedSport *string
	InvalidValue *string
}

func (err *ErrUnexpectedProviderResponse) Error() string {
	return fmt.Sprintf("got unexpected response from provider")
}