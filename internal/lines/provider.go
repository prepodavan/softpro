package lines

import (
	"go.uber.org/zap"
	"sync"
)

type Provider struct {
	config  *ProviderConfig
	wg      *sync.WaitGroup
	logger  *zap.Logger
	pullers []*linePuller
}

func (pro *Provider) Close() {
	pro.logger.Info("closing")
	for _, puller := range pro.pullers {
		puller.close()
	}

	pro.wg.Wait()
	pro.logger.Info("closed")
}

func (pro *Provider) Run() {
	for _, puller := range pro.pullers {
		go puller.run()
	}

	pro.logger.Info("running")
}