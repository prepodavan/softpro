package lines

import (
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
	"net/http"
	"net/http/httptest"
	"softpro/internal/domain/models"
	"softpro/internal/testutils"
	"strings"
	"sync"
	"testing"
	"time"
)

func TestPulling(t *testing.T) {
	t.Parallel()
	logger := testutils.TestingLogger(t)
	expectedEvent := &models.Line{
		Sport: "baseball",
		Value: 1.11,
	}

	res := makeResponse(strings.ToUpper(expectedEvent.Sport), fmt.Sprintf("%f", expectedEvent.Value))
	server := newServer().setJSON(res)
	defer server.httpServer.Close()
	client := server.httpServer.Client()
	storage := &eventStorage{}
	puller := newLinePuller(server.httpServer.URL, &sync.WaitGroup{}, logger, &pullerConfig{
		Sport:    expectedEvent.Sport,
		Interval: time.Millisecond,
		Storage:  storage,
		Client:   client,
	})

	err := puller.pull()
	assert.Nil(t, err)
	assert.NotNil(t, storage.saved)
	assert.Equal(t, expectedEvent.Value, storage.saved.Value)
	assert.Equal(t, expectedEvent.Sport, storage.saved.Sport)

	storage.saved = nil

	server.response = []byte("not json")
	err = puller.pull()
	assert.NotNil(t, err)
	assert.Nil(t, storage.saved)

	client.Timeout = time.Millisecond
	server.sleepTime = time.Second
	err = puller.pull()
	assert.NotNil(t, err)
	assert.Nil(t, storage.saved)
}

func TestParsing(t *testing.T) {
	t.Parallel()
	logger := testutils.TestingLogger(t)
	sport := "baseball"
	expectedValue := 1.11
	puller := newLinePuller("", &sync.WaitGroup{}, logger, &pullerConfig{Sport: sport})

	event, err := puller.parse(makeResponse(strings.ToUpper(sport), fmt.Sprintf("%f", expectedValue)))
	assert.Nil(t, err)
	assert.Equal(t, event.Sport, sport)
	assert.Equal(t, event.Value, expectedValue)

	event, err = puller.parse(makeResponse(strings.ToUpper(sport), ""))
	assert.NotNil(t, err)
	unexpected, ok := err.(*ErrUnexpectedProviderResponse)
	assert.True(t, ok)
	assert.NotNil(t, unexpected.InvalidValue)

	event, err = puller.parse(makeResponse("blah", fmt.Sprintf("%f", expectedValue)))
	assert.NotNil(t, err)
	unexpected, ok = err.(*ErrUnexpectedProviderResponse)
	assert.True(t, ok)
	assert.NotNil(t, unexpected.ExpectedSport)
	assert.Equal(t, *unexpected.ExpectedSport, strings.ToUpper(sport))
}

func TestFetching(t *testing.T) {
	t.Parallel()
	logger := testutils.TestingLogger(t)
	sport := "baseball"
	expectedResponse := makeResponse(strings.ToUpper(sport), "1.11")

	server := newServer().setJSON(expectedResponse)
	defer server.httpServer.Close()
	client := server.httpServer.Client()
	storage := &eventStorage{}
	puller := newLinePuller(server.httpServer.URL, &sync.WaitGroup{}, logger, &pullerConfig{
		Sport:    sport,
		Interval: time.Millisecond,
		Storage:  storage,
		Client:   client,
	})

	actualResponse, err := puller.fetch()
	assert.Nil(t, err)
	assert.NotNil(t, actualResponse)
	assert.True(t, expectedResponse.equals(actualResponse))

	server.response = []byte("not json")
	actualResponse, err = puller.fetch()
	assert.Nil(t, actualResponse)
	assert.NotNil(t, err)
	_, ok := err.(*ErrLineDecoding)
	assert.True(t, ok)

	client.Timeout = time.Millisecond
	server.sleepTime = time.Second
	actualResponse, err = puller.fetch()
	assert.Nil(t, actualResponse)
	assert.NotNil(t, err)
	_, ok = err.(*ErrGetLine)
	assert.True(t, ok)
}

type providerServer struct {
	httpServer *httptest.Server
	response   []byte
	logger     *zap.Logger
	status     int
	sleepTime  time.Duration
}

func (s *providerServer) handler(writer http.ResponseWriter, request *http.Request) {
	time.Sleep(s.sleepTime)
	writer.WriteHeader(s.status)
	if s.response == nil {
		return
	}

	_, err := writer.Write(s.response)
	if err != nil && s.logger != nil {
		s.logger.Error("cant write response", zap.Error(err), zap.ByteString("response", s.response))
	}
}

func (s *providerServer) setValue(sport string, val string) *providerServer {
	b, _ := json.Marshal(&providerResponse{
		Lines: map[string]string{
			sport: val,
		},
	})

	s.response = b
	return s
}

func (s *providerServer) setJSON(v interface{}) *providerServer {
	b, _ := json.Marshal(v)
	s.response = b
	return s
}

func newServer() *providerServer {
	ps := &providerServer{}
	hs := httptest.NewServer(http.HandlerFunc(ps.handler))
	ps.httpServer = hs
	ps.status = http.StatusOK
	ps.sleepTime = 1
	return ps
}

type eventStorage struct {
	saved *models.Line
	err   error
}

func (s *eventStorage) Save(event models.Line) error {
	s.saved = &event
	return s.err
}

func (res *providerResponse) equals(other *providerResponse) (eq bool) {
	eq = len(res.Lines) == len(other.Lines)
	if !eq {
		return
	}

	for k, v := range res.Lines {
		if _, eq = other.Lines[k]; !eq {
			return
		}

		eq = v == other.Lines[k]
		if !eq {
			return
		}
	}

	return
}

func makeResponse(key, val string) *providerResponse {
	return &providerResponse{Lines: map[string]string{key: val}}
}
