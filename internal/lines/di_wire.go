package lines

import (
	"github.com/google/wire"
	"go.uber.org/zap"
	"net/http"
	"reflect"
	"sync"
	"time"
)

var ProvidePuller = wire.NewSet(NewLinesProvider)

type SoccerRepository SportLineStorage
type FootballRepository SportLineStorage
type BaseballRepository SportLineStorage

func NewLinesProvider(
	logger *zap.Logger,
	config *ProviderConfig,
	sr SoccerRepository,
	fr FootballRepository,
	br BaseballRepository,
) (*Provider, func()) {
	provider := &Provider{
		logger:  logger.With(zap.String("type", reflect.TypeOf(&Provider{}).String())),
		config:  config,
		wg:      &sync.WaitGroup{},
		pullers: make([]*linePuller, len(config.Lines)),
	}

	repos := map[string]SportLineStorage{
		"soccer": sr,
		"football": fr,
		"baseball": br,
	}

	for i := range config.Lines {
		interval := time.Duration(config.Lines[i].IntervalInSeconds) * time.Second
		provider.pullers[i] = newLinePuller(config.BaseURL, provider.wg, logger, &pullerConfig{
			Sport:    config.Lines[i].Sport,
			Interval: interval,
			Storage:  repos[config.Lines[i].Sport],
			Client:   &http.Client{Timeout: interval},
		})
	}

	return provider, provider.Close
}