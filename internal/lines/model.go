//go:generate easyjson
package lines

//easyjson:json
type providerResponse struct {
	Lines map[string]string `json:"lines"`
}
