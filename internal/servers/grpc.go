package servers

import (
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"softpro/internal/services/liner"
)

func NewGrpcServer(logger *zap.Logger, subscriber *liner.LinesService) (*grpc.Server, func()) {
	srv := grpc.NewServer()
	liner.RegisterLinerServer(srv, subscriber)
	return srv, func() {
		logger.Info("closing grpc")
		srv.GracefulStop()
		logger.Info("grpc closed")
	}
}