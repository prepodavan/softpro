package servers

import (
	"go.uber.org/zap"
	"net/http"
	"softpro/internal/configs"
	"softpro/internal/services/monitor"
)

func NewHttpServer(logger *zap.Logger, cfg *configs.Net, mon *monitor.Monitor) (*http.Server, func()) {
	srv := &http.Server{Addr: cfg.HttpAddr}
	mux := http.NewServeMux()
	mux.Handle("/ready", mon)
	srv.Handler = mux
	return srv, func() {
		if err := srv.Close(); err != nil && err != http.ErrServerClosed {
			logger.Error("close http error", zap.Error(err))
		} else {
			logger.Info("http closed")
		}
	}
}