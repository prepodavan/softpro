package servers

import "github.com/google/wire"

var ProviderServers = wire.NewSet(NewHttpServer, NewGrpcServer)