package monitor

import "github.com/google/wire"

var ProviderOfMonitor = wire.NewSet(
	wire.Struct(new(Monitor), "Pool", "Logger"),
)