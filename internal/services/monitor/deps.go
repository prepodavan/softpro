package monitor

type ConnectionPool interface {
	Ping() []error
}
