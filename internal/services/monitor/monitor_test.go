package monitor

import (
	"encoding/json"
	"errors"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"softpro/internal/testutils"
	"strings"
	"testing"
)

func TestMonitor_ServeHTTP(t *testing.T) {
	t.Parallel()
	logger := testutils.TestingLogger(t)
	dbs := &dbPool{errors: []error{}}
	suites := []struct {
		errors []error
		responseCode int
	}{
		{
			errors: nil,
			responseCode: http.StatusOK,
		},
		{
			errors: []error{},
			responseCode: http.StatusOK,
		},
		{
			errors: []error{errors.New("1")},
			responseCode: http.StatusServiceUnavailable,
		},
		{
			errors: []error{errors.New("2"), errors.New("3")},
			responseCode: http.StatusServiceUnavailable,
		},
	}

	for _, suite := range suites {
		dbs.errors = suite.errors
		mon := &Monitor{Pool: dbs, Logger: logger}
		recorder := httptest.NewRecorder()
		mon.ServeHTTP(recorder, httptest.NewRequest("GET", "/ready", strings.NewReader("")))
		assert.Equal(t, recorder.Code, suite.responseCode)
		if len(suite.errors) == 0 {
			assert.Len(t, recorder.Body.Bytes(), 0)
			continue
		}

		var res response
		assert.Nil(t, json.Unmarshal(recorder.Body.Bytes(), &res))
		res.assertErrors(t, suite.errors)
	}
}

type response struct {
	Errors []string `json:"info"`
}

func (res *response) assertErrors(t *testing.T, errors []error) {
	assert.Len(t, res.Errors, len(errors))
	for _, errStr := range res.Errors {
		found := false
		for _, err := range errors {
			if err.Error() == errStr {
				found = true
				break
			}
		}

		assert.True(t, found)
	}
}

type dbPool struct {
	errors []error
}

func (p *dbPool) Ping() []error {
	return p.errors
}