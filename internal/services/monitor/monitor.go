package monitor

import (
	"encoding/json"
	"go.uber.org/zap"
	"net/http"
)

type Monitor struct {
	Pool   ConnectionPool
	Logger *zap.Logger
}

func (s *Monitor) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	errors := make([]string, 0)
	if pingErrors := s.Pool.Ping(); len(pingErrors) != 0 {
		for _, err := range pingErrors {
			errors = append(errors, err.Error())
		}
	}

	if len(errors) == 0 {
		writer.WriteHeader(http.StatusOK)
		return
	}

	writer.WriteHeader(http.StatusServiceUnavailable)
	errorInfoResponse := make(map[string]interface{})
	errorInfoResponse["info"] = errors
	bytes, err := json.Marshal(errorInfoResponse)
	if err != nil {
		//TODO log
		return
	}

	_, err = writer.Write(bytes)
	if err != nil {
		//TODO log
		return
	}
}
