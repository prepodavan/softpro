package liner

import (
	"go.uber.org/zap"
)

type LinesService struct {
	UnimplementedLinerServer
	sportLines linesPool
	logger     *zap.Logger
}

func (s *LinesService) SubscribeOnSportLines(stream Liner_SubscribeOnSportLinesServer) error {
	return newSubscriber(s, stream).Process()
}
