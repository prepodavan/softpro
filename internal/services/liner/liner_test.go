package liner

import (
	"context"
	"softpro/internal/testutils"

	"math/rand"
	"net"
	"softpro/internal/domain/models"
	"softpro/internal/testutils/mocks"
	"testing"
	"time"

	"github.com/golang/protobuf/ptypes"
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"

	"google.golang.org/grpc"
	"google.golang.org/grpc/test/bufconn"
)

func TestLinesService_SubscribeOnSportLines(t *testing.T) {
	t.Parallel()
	logger := testutils.TestingLogger(t)
	bufSize := 1024 * 1024
	rand.Seed(time.Now().UnixNano())
	repos := reposPool(map[string]repository{
		"soccer":   mocks.NewInMemorySportLinesRepository(),
		"football": mocks.NewInMemorySportLinesRepository(),
		"baseball": mocks.NewInMemorySportLinesRepository(),
	})

	listener := bufconn.Listen(bufSize)
	server := grpc.NewServer()
	service := &LinesService{
		logger: logger,
		sportLines: map[string]SportLineRepository{
			"soccer":   repos["soccer"],
			"football": repos["football"],
			"baseball": repos["baseball"],
		},
	}

	RegisterLinerServer(server, service)
	go func() {
		assert.Nil(t, server.Serve(listener))
	}()

	ctx, _ := context.WithTimeout(context.Background(), time.Hour*24*31*12*5)
	dialer := buildBufNetDialer(listener)
	conn, err := grpc.DialContext(ctx, "bufnet", grpc.WithContextDialer(dialer), grpc.WithInsecure())
	assert.Nil(t, err)
	client := NewLinerClient(conn)
	stream, err := client.SubscribeOnSportLines(ctx)
	assert.Nil(t, err)
	rcvr := &receiver{
		logger:    logger,
		repos:     repos,
		stream:    stream,
		responses: make([]map[string]float64, 0),
	}

	st := makeStats()
	logger.Debug("initial stats", st.fields()...)
	repos.save(st)
	err = stream.Send(makeRequest(time.Millisecond*50, "football", "soccer"))
	rcvr.assertReceive(t, 3, 2)
	err = stream.Send(makeRequest(time.Millisecond*100, "football", "soccer"))
	assert.Nil(t, err)
	rcvr.assertReceive(t, 2, 2)
	err = stream.Send(makeRequest(time.Millisecond*200, "baseball", "soccer"))
	assert.Nil(t, err)
	rcvr.responses = make([]map[string]float64, 0)
	rcvr.assertReceive(t, 2, 2)
}

type receiver struct {
	responses []map[string]float64
	repos     reposPool
	stream    Liner_SubscribeOnSportLinesClient
	logger    *zap.Logger
}

func (rcvr *receiver) assertReceive(t *testing.T, iterations, sports int) {
	for i := 0; i < iterations; i++ {
		if i != 0 {
			st := makeStats()
			rcvr.logger.Debug("made", st.fields()...)
			rcvr.repos.save(st)
		}

		received, err := rcvr.stream.Recv()
		assert.Nil(t, err)
		assert.NotNil(t, received)
		assert.Len(t, received.Stats, sports)
		rcvr.responses = append(rcvr.responses, received.Stats)
		rcvr.repos.assert(t, rcvr.responses...)
		rcvr.logger.Debug("asserted", zap.Any("responses", rcvr.responses), zap.Bool(
			"failed",
			t.Failed(),
		))
	}
}

type reposPool map[string]repository

func (p reposPool) assert(t *testing.T, allStats ...map[string]float64) {
	summed := make(map[string]float64)
	for _, stats := range allStats {
		for sport, value := range stats {
			summed[sport] += value
		}
	}

	for sport, actual := range summed {
		expected, _ := p[sport].GetLast()
		assert.Equal(t, expected.Value, actual)
	}
}

func (p reposPool) save(st *linesStats) {
	p["soccer"].Save(st.Soccer)
	p["football"].Save(st.Football)
	p["baseball"].Save(st.Baseball)
}

type linesStats struct {
	Soccer   models.Line `json:"soccer"`
	Football models.Line `json:"football"`
	Baseball models.Line `json:"baseball"`
}

func (ls *linesStats) fields() []zap.Field {
	return []zap.Field{
		zap.Float64("soccer", ls.Soccer.Value),
		zap.Float64("baseball", ls.Baseball.Value),
		zap.Float64("football", ls.Football.Value),
	}
}

func makeStats() *linesStats {
	return &linesStats{
		Soccer:   makeLine("soccer"),
		Football: makeLine("football"),
		Baseball: makeLine("baseball"),
	}
}

func makeLine(sport string) models.Line {
	return models.Line{
		Sport:   sport,
		Value:   float64(rand.Intn(6) - 3),
		Instant: time.Now(),
	}
}

func makeRequest(dur time.Duration, sports ...string) *SubscribeOnSportLinesRequest {
	return &SubscribeOnSportLinesRequest{
		Sports:   sports,
		Interval: ptypes.DurationProto(dur),
	}
}

func buildBufNetDialer(listener *bufconn.Listener) func(context.Context, string) (net.Conn, error) {
	return func(ctx context.Context, s string) (net.Conn, error) {
		return listener.Dial()
	}
}

type repository interface {
	SportLineRepository
	Save(line models.Line) (err error)
}
