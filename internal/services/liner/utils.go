package liner

func isSameSports(r1 *SubscribeOnSportLinesRequest, r2 *SubscribeOnSportLinesRequest) bool {
	if len(r1.Sports) != len(r2.Sports) {
		return false
	}

	for _, s1 := range r1.Sports {
		contains := false
		for _, s2 := range r2.Sports {
			if s1 == s2 {
				contains = true
				break
			}
		}

		if !contains {
			return false
		}
	}

	return true
}
