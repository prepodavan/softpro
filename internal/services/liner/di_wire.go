package liner

import (
	"github.com/google/wire"
	"go.uber.org/zap"
)

var ProvideSubscriber = wire.NewSet(
	NewLinesService,
)

type SoccerRepo SportLineRepository
type FootballRepo SportLineRepository
type BaseballRepo SportLineRepository

func NewLinesService(
	logger *zap.Logger,
	sr SoccerRepo,
	fr FootballRepo,
	br BaseballRepo,
) *LinesService {
	return &LinesService{
		logger: logger,
		sportLines: map[string]SportLineRepository{
			"soccer":   sr,
			"football": fr,
			"baseball": br,
		},
	}
}
