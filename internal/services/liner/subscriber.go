package liner

import (
	"context"
	"github.com/golang/protobuf/ptypes"
	"go.uber.org/zap"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"io"
	"time"
)

type subscriber struct {
	sportLines    linesPool
	logger        *zap.Logger
	stream        Liner_SubscribeOnSportLinesServer
	request       *SubscribeOnSportLinesRequest
	previousStats map[string]float64
	ticker        *time.Ticker
	config        *subscriberConfig
	interval      time.Duration
}

type subscriberConfig struct {
	minInterval time.Duration
}

func newSubscriber(service *LinesService, stream Liner_SubscribeOnSportLinesServer) *subscriber {
	return &subscriber{
		sportLines: service.sportLines,
		logger:     service.logger,
		stream:     stream,
		config:     &subscriberConfig{minInterval: time.Microsecond * 500}, //TODO
	}
}

func (sub *subscriber) Process() error {
	defer sub.stopTicker()
	requests := make(chan *SubscribeOnSportLinesRequest, 2)
	listenErrors := make(chan error, 2)
	initial, err := sub.receive()
	if initial == nil {
		return err //TODO
	}

	sub.logger.Debug("handle initial")
	err = sub.handle(initial)
	sub.logger.Debug("handled initial", zap.Error(err))
	if err != nil {
		return err
	}

	go sub.listen(requests, listenErrors)
	for {
		select {
		case req := <-requests:
			sub.logger.Debug("handle req", zap.Any("req", req))
			err = sub.handle(req)
		case <-sub.ticker.C:
			sub.logger.Debug(
				"ticked",
				zap.Strings("sports", sub.request.Sports),
				zap.Any("interval", sub.request.Interval.String()),
			)

			err = sub.onTick()
		case err = <-listenErrors:
		}

		if err != nil {
			return err
		}
	}
}

func (sub *subscriber) stopTicker() {
	if sub.ticker != nil {
		sub.ticker.Stop()
	}
}

func (sub *subscriber) onTick() (err error) {
	ctx, cancel := context.WithTimeout(context.Background(), sub.interval)
	defer cancel()
	select {
	case err = <-sub.respond():
	case <-ctx.Done():
		err = status.Error(codes.Internal, "timeout exceed") //TODO
	}

	return
}

func (sub *subscriber) respond() (out chan error) {
	out = make(chan error)
	go func(output chan error) {
		res := &SubscribeOnSportLinesResponse{Stats: make(map[string]float64)}
		backupStats := make(map[string]float64)
		for _, sport := range sub.request.Sports {
			last, err := sub.sportLines[sport].GetLast()
			if err != nil {
				output <- err //TODO
				return
			}

			res.Stats[sport] = last.Value
			backupStats[sport] = last.Value
		}

		if sub.previousStats != nil {
			for sport := range res.Stats {
				res.Stats[sport] -= sub.previousStats[sport]
			}
		}

		sub.previousStats = backupStats
		sub.logger.Debug("sending", zap.Any("payload.stats", res.Stats))
		err := sub.stream.Send(res)
		out <- err //TODO
	}(out)

	return
}

func (sub *subscriber) handle(request *SubscribeOnSportLinesRequest) error {
	sub.stopTicker()
	for _, sport := range request.Sports {
		if _, ok := sub.sportLines[sport]; !ok {
			return status.Errorf(codes.NotFound, "sport %s is unknown", sport) //TODO
		}
	}

	duration, err := ptypes.Duration(request.Interval)
	if err != nil {
		return status.Error(codes.InvalidArgument, "invalid interval") //TODO
	}

	if duration < sub.config.minInterval {
		return status.Errorf(
			codes.InvalidArgument,
			"interval %d is less than min which is %d",
			duration,
			sub.config.minInterval,
		) //TODO
	}

	sub.interval = duration
	sub.ticker = time.NewTicker(duration)
	if sub.request != nil && !isSameSports(request, sub.request) {
		sub.previousStats = nil
	}

	sub.request = request
	return nil
}

func (sub *subscriber) receive() (request *SubscribeOnSportLinesRequest, err error) {
	sub.logger.Debug("receiving")
	request, err = sub.stream.Recv()
	if err == io.EOF {
		sub.logger.Debug("eof") //TODO
		err = nil
		return
	}

	if err != nil {
		sub.logger.Error("err recv", zap.Error(err)) //TODO
		return
	}

	if request == nil {
		err = status.Errorf(codes.Internal, "internal stream error") //TODO
		return
	}

	sub.logger.Debug("req", zap.Any("req", request)) //TODO
	return
}

func (sub *subscriber) listen(requestsOut chan *SubscribeOnSportLinesRequest, errorsOut chan error) {
	sub.logger.Debug("listening")
	for {
		req, err := sub.receive()
		if err != nil {
			errorsOut <- err
			return
		} else if req == nil {
			return
		} else {
			requestsOut <- req
		}
	}
}
