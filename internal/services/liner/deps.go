package liner

import (
	"softpro/internal/domain/models"
)

type SportLineRepository interface {
	GetLast() (last models.Line, err error)
}
