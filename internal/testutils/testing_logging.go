package testutils

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"os"
	"testing"

	"go.uber.org/zap"
)

var (
	ErrNoConfigProvided = errors.New("no config path")
	loggerConfigPath    = os.Getenv("TEST_LOGGER_CONFIG_PATH")
)

func TestingLogger(t *testing.T) *zap.Logger {
	t.Helper()
	if len(loggerConfigPath) == 0 {
		t.Fatal(ErrNoConfigProvided)
	}

	f, err := ioutil.ReadFile(loggerConfigPath)
	if err != nil {
		t.Fatal(err)
	}

	var c zap.Config
	err = json.Unmarshal(f, &c)
	if err != nil {
		t.Fatal(err)
	}

	logger, err := c.Build()
	if err != nil {
		t.Fatal(err)
	}

	return logger
}
