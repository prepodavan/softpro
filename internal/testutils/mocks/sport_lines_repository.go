package mocks

import (
	"softpro/internal/domain/models"
	"sync"
)

type SportLinesRepositoryInMemory struct {
	mx     sync.RWMutex
	latest models.Line
}

func NewInMemorySportLinesRepository() *SportLinesRepositoryInMemory {
	return &SportLinesRepositoryInMemory{}
}

func (r *SportLinesRepositoryInMemory) Save(line models.Line) (err error) {
	r.mx.Lock()
	r.latest = line
	r.mx.Unlock()
	return
}

func (r *SportLinesRepositoryInMemory) GetLast() (last models.Line, err error) {
	r.mx.RLock()
	last = r.latest
	r.mx.RUnlock()
	return
}
