package mocks

import (
	"softpro/internal/testutils"
	"testing"
)

func TestSportLinesRepositoryInMemory(t *testing.T) {
	t.Parallel()
	logger := testutils.TestingLogger(t)
	testutils.TestLineRepository(t, 10, "sport", logger, NewInMemorySportLinesRepository())
}
