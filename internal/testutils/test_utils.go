package testutils

import (
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
	"math/rand"
	"softpro/internal/domain/models"
	"testing"
	"time"
)

func TestLineRepository(t *testing.T, testSize int, sport string, logger *zap.Logger, repo interface {
	Save(models.Line) error
	GetLast() (last models.Line, err error)
}) {
	t.Helper()
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < testSize; i++ {
		now := time.Now()
		line := models.Line{
			Sport:   sport,
			Value:   rand.Float64(),
			Instant: now,
		}

		assert.Nil(t, repo.Save(line))
		actual, err := repo.GetLast()
		assert.Nil(t, err)
		assert.Equal(t, line.Sport, actual.Sport)
		assert.Equal(t, line.Value, actual.Value)
		assert.True(t, actual.Instant.Equal(line.Instant))
	}
}
