package app

import (
	"github.com/google/wire"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"net/http"
	"softpro/internal/configs"
	"softpro/internal/databases"
	"softpro/internal/lines"
)

var ProvideApp = wire.NewSet(
	wire.Struct(new(App), "Cfg", "Logger", "Http", "Grpc", "Pool", "Puller"),
)

func NewApp(
	logger *zap.Logger,
	puller *lines.Provider,
	dbPool *databases.ConnectionPool,
	cfg *configs.Full,
	httpServer *http.Server,
	grpcServer *grpc.Server,
) (*App, func()) {
	//mainApp := &App{
	//	Logger: logger,
	//	Http:   httpServer,
	//	Grpc:   grpcServer,
	//	Cfg:    cfg,
	//	Pool:   dbPool,
	//	Puller: puller,
	//}

	return nil, nil
}