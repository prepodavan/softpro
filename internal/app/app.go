package app

import (
	"errors"
	"net"
	"net/http"
	"softpro/internal/configs"
	"softpro/internal/databases"
	"softpro/internal/lines"
	"sync"
	"time"

	"go.uber.org/zap"
	"google.golang.org/grpc"
)

var ErrPoolConnect = errors.New("cant connect db pool")

type App struct {
	WG     sync.WaitGroup
	Logger *zap.Logger
	Http   *http.Server
	Grpc   *grpc.Server
	Cfg    *configs.Full
	Pool   *databases.ConnectionPool
	Puller *lines.Provider
}

func (instance *App) ServeHttp(onErr chan error) {
	instance.WG.Add(1)
	defer instance.WG.Done()
	instance.Logger.Info("serving http", zap.String("addr", instance.Cfg.Net.HttpAddr))
	err := instance.Http.ListenAndServe()
	if err != nil && err != http.ErrServerClosed {
		instance.Logger.Error("http listen err", zap.Error(err)) //TODO
		onErr <- err
	}
}

func (instance *App) ServeGrpc(onErr chan error) {
	instance.WG.Add(1)
	defer instance.WG.Done()
	lis, err := net.Listen("tcp", instance.Cfg.Net.GrpcAddr)
	if err != nil {
		instance.Logger.Error("grpc listen error", zap.Error(err)) //TODO
		onErr <- err
	}

	err = instance.Grpc.Serve(lis)
	if err != nil && err != grpc.ErrServerStopped {
		instance.Logger.Error("grpc serve error", zap.Error(err)) //TODO
		onErr <- err
	}
}

func (instance *App) ConnectPool(onErr chan error, onConnect chan struct{}) {
	instance.WG.Add(1)
	defer instance.WG.Done()
	var errs []error
	for i := 0; i < 5; i++ {
		errs = instance.Pool.Connect()
		if len(errs) == 0 {
			onConnect <- struct{}{}
			return
		}

		for _, err := range errs {
			instance.Logger.Error("connect db errors", zap.Error(err), zap.Int("attempt", i)) //TODO
		}

		time.Sleep(5 * time.Second)
	}

	onErr <- ErrPoolConnect
}
