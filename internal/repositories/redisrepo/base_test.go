package redisrepo

import (
	"github.com/alicebob/miniredis"
	"github.com/go-redis/redis"
	"github.com/stretchr/testify/assert"
	"softpro/internal/domain/models"
	"softpro/internal/repositories"
	"softpro/internal/testutils"
	"testing"
)

func TestBaseSportLineRepository(t *testing.T) {
	t.Parallel()
	logger := testutils.TestingLogger(t)
	sport := "title"
	repo := &baseSportLineRepository{
		sportTitle: sport,
		logger:     logger,
		dbs:        &poolWithRedis{},
	}

	err := repo.Save(models.Line{})
	assert.Equal(t, err, repositories.ErrNoDB)

	pool := newPool(t)
	defer pool.client.Close()
	defer pool.server.Close()
	repo.dbs = pool
	testutils.TestLineRepository(t, 10, sport, logger, repo)
}

func newPool(t *testing.T) *poolWithRedis {
	server, err := miniredis.Run()
	assert.Nil(t, err)

	client := redis.NewClient(&redis.Options{
		Network: "tcp",
		Addr:    server.Addr(),
		DB:      0,
	})

	return &poolWithRedis{
		client: client,
		server: server,
	}
}

type poolWithRedis struct {
	client *redis.Client
	server *miniredis.Miniredis
}

func (p *poolWithRedis) GetRedis() *redis.Client {
	return p.client
}
