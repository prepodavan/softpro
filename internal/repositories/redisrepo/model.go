//go:generate easyjson
package redisrepo

import "encoding/json"

//easyjson:json
type line struct {
	Value   float64 `json:"value"`
	Instant int64   `json:"instant"`
}

func (le *line) MarshalBinary() (data []byte, err error) {
	return json.Marshal(le)
}

func (le *line) UnmarshalBinary(data []byte) error {
	return json.Unmarshal(data, le)
}
