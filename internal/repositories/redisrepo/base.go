package redisrepo

import (
	"github.com/go-redis/redis"
	"go.uber.org/zap"
	"softpro/internal/domain/models"
	"softpro/internal/repositories"
	"time"
)

type baseSportLineRepository struct {
	sportTitle string
	dbs        ConnectionPool
	logger     *zap.Logger
}

func (repo *baseSportLineRepository) GetLast() (last models.Line, err error) {
	db := repo.dbs.GetRedis()
	if db == nil {
		err = repositories.ErrNoDB
		return
	}

	last, err = repo.getLast(db)
	return
}

func (repo *baseSportLineRepository) Save(line models.Line) error {
	db := repo.dbs.GetRedis()
	if db == nil {
		return repositories.ErrNoDB
	}

	return repo.save(db, line)
}

func (repo *baseSportLineRepository) getLast(db *redis.Client) (last models.Line, err error) {
	var result []byte
	result, err = db.Get(repo.sportTitle).Bytes()
	if err != nil {
		//TODO
		return
	}

	lineValue := &line{}
	err = lineValue.UnmarshalBinary(result)
	if err != nil {
		//TODO
		return
	}

	last = models.Line{
		Sport:   repo.sportTitle,
		Value:   lineValue.Value,
		Instant: time.Unix(0, lineValue.Instant),
	}

	return
}

func (repo *baseSportLineRepository) save(db *redis.Client, model models.Line) (err error) {
	cmd := db.Set(repo.sportTitle, &line{
		Value:   model.Value,
		Instant: model.Instant.UnixNano(),
	}, 0)

	if err = cmd.Err(); err != nil {
		//TODO
		return
	}

	return
}
