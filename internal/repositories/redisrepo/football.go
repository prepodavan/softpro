package redisrepo

import "go.uber.org/zap"

type FootballRepository struct {
	baseSportLineRepository
}

func NewFootballRepository(pool ConnectionPool, logger *zap.Logger) *FootballRepository {
	return &FootballRepository{
		baseSportLineRepository: baseSportLineRepository{
			dbs:        pool,
			sportTitle: "football",
			logger:     logger,
		},
	}
}
