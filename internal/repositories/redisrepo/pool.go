package redisrepo

import "github.com/go-redis/redis"

type ConnectionPool interface {
	GetRedis() *redis.Client
}