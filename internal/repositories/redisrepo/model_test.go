package redisrepo

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestLineEvent_Marshaling(t *testing.T) {
	ev := &line{
		Value:   12.1,
		Instant: 13,
	}

	b, err := ev.MarshalBinary()
	assert.Nil(t, err)
	assert.NotNil(t, b)

	parsed := &line{}
	err = parsed.UnmarshalBinary(b)
	assert.Nil(t, err)
	assert.Equal(t, parsed.Value, ev.Value)
	assert.Equal(t, parsed.Instant, ev.Instant)
}
