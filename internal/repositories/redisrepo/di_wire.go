package redisrepo

import "github.com/google/wire"

var ProviderRedisRepos = wire.NewSet(
	NewBaseballRepository,
	NewFootballRepository,
)