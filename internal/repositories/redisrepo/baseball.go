package redisrepo

import "go.uber.org/zap"

type BaseballRepository struct {
	baseSportLineRepository
}

func NewBaseballRepository(pool ConnectionPool, logger *zap.Logger) *BaseballRepository {
	return &BaseballRepository{
		baseSportLineRepository: baseSportLineRepository{
			dbs:        pool,
			sportTitle: "baseball",
			logger:     logger,
		},
	}
}
