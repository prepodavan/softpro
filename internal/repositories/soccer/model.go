//go:generate reform
package soccer

import "time"

//reform:soccer_line
type soccerLine struct {
	ID        uint64    `reform:"id,pk"`
	Value     float64   `reform:"value"`
	CreatedAt time.Time `reform:"created_at"`
}
