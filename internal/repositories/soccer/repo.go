package soccer

import (
	"errors"
	"go.uber.org/zap"
	"gopkg.in/reform.v1"
	"gopkg.in/reform.v1/dialects/postgresql"
	"softpro/internal/domain/models"
	"softpro/internal/repositories"
)

const Sport = "soccer"

var dialect = postgresql.Dialect

type Repository struct {
	DBs    DBPool
	Logger *zap.Logger
}

func (repo *Repository) GetLast() (last models.Line, err error) {
	db := repo.DBs.GetPostgres()
	if db == nil {
		err = repositories.ErrNoDB
	} else {
		logger := reform.NewPrintfLogger(func(format string, args ...interface{}) {})
		last, err = repo.getLast(reform.NewDB(db, dialect, logger))
	}

	return
}

func (repo *Repository) Save(event models.Line) (err error) {
	db := repo.DBs.GetPostgres()
	if db == nil {
		err = repositories.ErrNoDB
	} else {
		logger := reform.NewPrintfLogger(func(format string, args ...interface{}) {})
		err = repo.save(reform.NewDB(db, dialect, logger), event)
	}

	return
}

func (repo *Repository) getLast(db *reform.DB) (last models.Line, err error) {
	tail := "ORDER BY created_at DESC LIMIT 1"
	found, err := db.SelectOneFrom(soccerLineTable, tail)
	if err != nil {
		//TODO
		return
	}

	if line, ok := found.(*soccerLine); !ok {
		err = errors.New("returned wrong type") //TODO
	} else {
		last = models.Line{
			Sport:   Sport,
			Value:   line.Value,
			Instant: line.CreatedAt,
		}
	}

	return
}

func (repo *Repository) save(db *reform.DB, event models.Line) (err error) {
	err = db.Save(&soccerLine{
		Value:     event.Value,
		CreatedAt: event.Instant,
	})

	if err != nil {
		//TODO
	}

	return
}
