package soccer

import "database/sql"

type DBPool interface {
	GetPostgres() *sql.DB
}
