package repositories

import "errors"

var (
	ErrNoDB     = errors.New("no db provided")
	ErrNoEvents = errors.New("no events")
)
