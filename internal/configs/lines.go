package configs

import "time"

type Provider struct {
	BaseURL string
	Lines []*Line
}

type Line struct {
	Sport string
	Storage string
	Timeout time.Duration
	Interval time.Duration
}