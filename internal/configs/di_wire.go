package configs

import (
	"github.com/BurntSushi/toml"
	"github.com/google/wire"
	"go.uber.org/zap"
)

var ConfigProviders = wire.NewSet(
	ParseConfig,
	BuildLogger,
)

func ParseConfig(fp string) (*Full, error) {
	var cfg Full
	_, err := toml.DecodeFile(fp, &cfg)
	if err != nil {
		return nil, err
	}

	return &cfg, nil
}

func BuildLogger(cfg *Full) (logger *zap.Logger, err error) {
	logger, err = cfg.Logging.Build()
	return
}