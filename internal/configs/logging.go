package configs

import "go.uber.org/zap"

type Logging struct {
	zap.Config
}