package configs

type Full struct {
	DB      DB
	Net     Net
	Logging Logging
}