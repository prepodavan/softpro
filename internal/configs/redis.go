package configs

type Redis struct {
	DB int
	Addr string
	Password  string
}