package configs

type Pg struct {
	Host string
	User string
	DBName string
	SSLMode string
	Password string
}