package configs

import "softpro/internal/databases"

type DB struct {
	Pg    databases.PgConfig
	Redis databases.RedisConfig
}