package configs

import (
	"softpro/internal/lines"
	"time"
)

type Net struct {
	HttpAddr                   string
	GrpcAddr                   string
	ShutdownTimeoutNanoseconds time.Duration
	LinesProvider              lines.ProviderConfig
}
